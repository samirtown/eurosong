<?php

use Illuminate\Database\Seeder;

class WinnersVideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('winners_videos')->insert([
        'winner_id' =>  '1',
        'video_url' =>  'https://www.youtube.com/results?search_query=eurosong+winner+2000',
        'year' =>  '2000'
      ]);    }
}
