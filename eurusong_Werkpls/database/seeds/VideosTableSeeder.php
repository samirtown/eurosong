<?php

use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('videos')->insert([
        'url' =>  'https://www.youtube.com/results?search_query=eurosong+winner+2000',
      ]);
    }
}
