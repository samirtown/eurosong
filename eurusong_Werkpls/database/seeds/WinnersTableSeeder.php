<?php

use Illuminate\Database\Seeder;

class WinnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('winners')->insert([
        'id' =>  '1',
        'country' => 'Denmark'
      ]);    }
}
