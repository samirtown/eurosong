<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WinnersVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create("winners_videos", function(Blueprint $table){
        $table->integer('winner_id');
        $table->string('video_url');
        $table->integer("year");
        $table->foreign('winner_id')->references('id')->on('winners');
        $table->foreign('video_url')->references('url')->on('videos');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('winners_videos');
    }
}
