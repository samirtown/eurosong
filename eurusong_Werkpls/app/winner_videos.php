<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class winner_videos extends Model
{
  protected $table = "winner_videos";
  public $timestamps = false;
  }
}
